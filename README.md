# Ollyo Image Gallery

Visit the [live version of this project](https://hasnahenamow-ollyo-task.netlify.app/).

## Installation

To run the project on your local machine, use npm:
- `npm install`
- `npm run start`

## Code Overview

The Image Gallery is designed using a Grid Layout and integrated with a package called `@dnd-kit` for dragging, sorting, and necessary animations to ensure a smooth user experience. The component folder contains all the files relevant to the Image Gallery. The `ImageGallery.jsx` file renders the `SortableImage.jsx` using a JavaScript Map.

### Complexities Encountered During Development

During development, I faced an issue where the `@dnd-kit` listeners were propagating to the input tag within that div, making the input checkbox unclickable. This issue has been resolved by using the `onMouseDown` event on the input tag to handle checkbox selection.
