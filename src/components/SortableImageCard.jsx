import { useSortable } from '@dnd-kit/sortable'
import { useState, useEffect } from 'react'
import { CSS } from '@dnd-kit/utilities'

const SortableImageCard = ({
  image,
  index,
  handleImageSelection,
  resetCheckboxes,
}) => {
  const { id, link } = image

  const [checked, setChecked] = useState(false)

  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id: image.id })

  const style = {
    transition,
    transform: CSS.Transform.toString(transform),
    zIndex: isDragging ? 1 : 'auto',
  }

  useEffect(() => {
    if (resetCheckboxes) {
      setChecked(false)
    }
  }, [resetCheckboxes])

  const handleCheckboxChange = () => {
    setChecked((prev) => !prev)
    handleImageSelection(id, !checked)
  }

  return (
    <div
      className={`relative group aspect-square border-2 rounded-md cursor-pointer bg-white ${
        index === 0 && 'col-span-2 row-span-2'
      } ${checked ? 'opacity-90' : ''}`}
      ref={setNodeRef}
      {...attributes}
      {...listeners}
      style={style}
    >
      <input
        type="checkbox"
        className={`w-6 aspect-square absolute z-10 top-4 left-4 cursor-pointer ${
          isDragging ? 'hidden' : checked ? 'block' : 'hidden'
        } ${
          !isDragging && 'group-hover:block transition-all duration-300 ease-in'
        }`}
        onMouseDown={(e) => {
          handleCheckboxChange()
        }}
        checked={checked}
      />
      <img
        src={link}
        alt="Your Image"
        className={`w-full h-auto object-cover rounded-md ${
          checked ? 'opacity-50' : ''
        }`}
      />
      {!isDragging && (
        <div className="absolute rounded-md inset-0 bg-black opacity-0 group-hover:opacity-50 transition-all duration-300 ease-in"></div>
      )}
    </div>
  )
}

export default SortableImageCard
