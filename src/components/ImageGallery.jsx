import { useState } from 'react'
import data from '../assets/data.json'
import { AddImage } from './AddImage'
import { DndContext, rectIntersection } from '@dnd-kit/core'
import {
  SortableContext,
  arrayMove,
  rectSortingStrategy,
} from '@dnd-kit/sortable'
import SortableImageCard from './SortableImageCard'

const ImageGallery = () => {
  const [images, setImages] = useState(data)
  const [imagesToDelete, setImagesToDelete] = useState([])
  const [resetCheckboxes, setResetCheckboxes] = useState(false)

  const handleImageSelection = (imageId, selected) => {
    if (selected) {
      setImagesToDelete((prevImages) => [...prevImages, imageId])
    } else {
      setImagesToDelete((prevImages) =>
        prevImages.filter((id) => id !== imageId)
      )
    }
  }

  const resetAllCheckboxes = () => {
    setResetCheckboxes(!resetCheckboxes)
    setImagesToDelete([])
  }

  const handleDelete = () => {
    const imagesToKeep = images.filter(
      (image) => !imagesToDelete.includes(image.id)
    )
    setImages(imagesToKeep)
    resetAllCheckboxes()
  }

  const onDragEnd = (event) => {
    const { active, over } = event
    if (active.id === over.id) {
      return
    }
    setImages((images) => {
      const oldIndex = images.findIndex((image) => image.id === active.id)
      const newIndex = images.findIndex((image) => image.id === over.id)
      return arrayMove(images, oldIndex, newIndex)
    })
  }

  const isAnyImageSelected = imagesToDelete.length > 0

  return (
    <div className="w-[75vw] m-auto bg-white border rounded-md">
      <div className="w-full h-20 border-b-2 flex items-center justify-between px-10">
        {isAnyImageSelected ? (
          <>
            <h1 className="text-2xl flex items-center">
              <input
                type="checkbox"
                checked={imagesToDelete.length}
                className="w-6 aspect-square me-2"
                onChange={resetAllCheckboxes}
              />
              {imagesToDelete.length === 1
                ? '1 File Selected'
                : `${imagesToDelete.length} Files Selected`}
            </h1>
            <h2
              className="text-xl text-red-500 hover:underline cursor-pointer"
              onClick={handleDelete}
            >
              Delete {imagesToDelete.length === 1 ? 'File' : 'Files'}
            </h2>
          </>
        ) : (
          <h1 className="text-2xl">Gallery</h1>
        )}
      </div>
      <div className="w-[70vw] m-auto py-10 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-4">
        <DndContext collisionDetection={rectIntersection} onDragEnd={onDragEnd}>
          <SortableContext items={images} strategy={rectSortingStrategy}>
            {images.length > 0 &&
              images.map((image, index) => (
                <SortableImageCard
                  key={image.id}
                  image={image}
                  index={index}
                  handleImageSelection={handleImageSelection}
                  resetCheckboxes={resetCheckboxes}
                />
              ))}
          </SortableContext>
        </DndContext>
        <AddImage />
      </div>
    </div>
  )
}

export default ImageGallery
