export const AddImage = () => {
  return (
    <div className="aspect-square flex flex-col items-center justify-center gap-6 border-2 border-dashed rounded-md cursor-pointer">
      <img
        className="w-8 aspect-square"
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Picture_icon_BLACK.svg/149px-Picture_icon_BLACK.svg.png?20180309172929"
        alt="image_thumbnail"
      />
      <h1 className="text-2xl">Add Image</h1>
    </div>
  )
}
